# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- Manifests to prepare a new Kubernetes cluster for the installation of a global Helm / Tiller installation
- Configuration and documentation for the `nginx-ingress` helm chart
  - GKE
  - kubeadm
- Configuration and documentation for the `cert-manager` helm chart
  - GKE
  - kubeadm
