# nginx-ingress

The configuration file for the `stable/cert-manager` helm chart.

## Usage

### Google Kubernetes engine (GKE)

To deploy a new `cert-manager` to a Google Kubernetes Engine (GKE) cluster, run:

```bash
$ helm install --tiller-namespace tiller --namespace cert-manager --name cert-manager --values config/cert-manager/gke.values.yaml stable/cert-manager
```

Verify your deployment by running:

```bash
$ kubectl get deploy -n cert-manager cert-manager
NAME           DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
cert-manager   3         3         3            3           1m
```

To upgrade an existing deployment, run:

```bash
$ helm upgrade --tiller-namespace tiller -f config/cert-manager/gke.values.yaml --namespace cert-manager cert-manager stable/cert-manager
```

### kubeadm

To deploy a new `cert-manager` to a kubeadm cluster, run:

```bash
$ helm install --tiller-namespace tiller --namespace cert-manager --name cert-manager --values config/cert-manager/kubeadm.values.yaml stable/cert-manager
```

Verify your deployment by running:

```bash
$ kubectl get deploy -n cert-manager cert-manager
NAME           DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
cert-manager   3         3         3            3           1m
```

To upgrade an existing deployment, run:

```bash
$ helm upgrade --tiller-namespace tiller -f config/cert-manager/kubeadm.values.yaml --namespace cert-manager cert-manager stable/cert-manager
```
