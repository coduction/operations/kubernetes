# nginx-ingress

The configuration file for the `stable/nginx-ingress` helm chart.

## Usage

### Google Kubernetes engine (GKE)

To deploy a new `nginx-ingress` to a Google Kubernetes Engine (GKE) cluster, run:

```bash
$ helm install --tiller-namespace tiller --namespace nginx-ingress --name nginx-ingress --values config/nginx-ingress/gke.values.yaml stable/nginx-ingress
```

Verify your deployment by running:

```bash
$ kubectl get svc -n nginx-ingress nginx-ingress-controller
NAME                       TYPE           CLUSTER-IP      EXTERNAL-IP      PORT(S)                      AGE
nginx-ingress-controller   LoadBalancer   10.11.246.123   35.242.211.186   80:30164/TCP,443:31509/TCP   20m
```

To upgrade an existing deployment, run:

```bash
$ helm upgrade --tiller-namespace tiller -f config/nginx-ingress/gke.values.yaml --namespace nginx-ingress nginx-ingress stable/nginx-ingress
```

### kubeadm

To deploy a new `nginx-ingress` to a `kubeadm` cluster, run:

```bash
$ helm install --tiller-namespace tiller --namespace nginx-ingress --name nginx-ingress --values config/nginx-ingress/kubeadm.values.yaml stable/nginx-ingress
```

Verify your deployment by running:

```bash
$ kubectl get svc -n nginx-ingress nginx-ingress-controller
NAME                       TYPE           CLUSTER-IP      EXTERNAL-IP      PORT(S)                      AGE
nginx-ingress-controller   LoadBalancer   10.11.246.123   35.242.211.186   80:30164/TCP,443:31509/TCP   20m
```

To upgrade an existing deployment, run:

```bash
$ helm upgrade --tiller-namespace tiller -f config/nginx-ingress/kubeadm.values.yaml --namespace nginx-ingress nginx-ingress stable/nginx-ingress
```
