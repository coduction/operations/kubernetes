# Kubernetes

This repository stores configuration and other data required for the usage and maintenance of Kubernetes.

## Folder structure

### Overview

```txt
├── manifest/
└── config/
```

### Folders

- `manifest`: basic static manifests for the setup of a Kubernetes cluster
- `config`: value configuration files for cluster-wide Helm chart installations

## License

This project is licensed under the terms of the [MIT license](https://mit-license.org).
