# Tiller

These manifests prepare a new Kubernetes cluster for the installation of a global Helm / Tiller installation.

## Requirements

The following programs should be installed in your path:

- `kubectl`
- `helm`

## Usage

To apply these manifests, navigate to this folder and run:

```bash
$ kubectl apply -f manifest/tiller
namespace/tiller created
serviceaccount/tiller created
clusterrolebinding.rbac.authorization.k8s.io/tiller created
```

Afterwards you can initialize Tiller by running:

```bash
$ helm init --service-account tiller --tiller-namespace tiller
$HELM_HOME has been configured at /home/ubuntu/.helm.

Tiller (the Helm server-side component) has been installed into your Kubernetes Cluster.

Please note: by default, Tiller is deployed with an insecure 'allow unauthenticated users' policy.
For more information on securing your installation see: https://docs.helm.sh/using_helm/#securing-your-helm-installation
Happy Helming!
```

Verify your deployment by running:

```bash
$ kubectl get deployments -n tiller tiller-deploy
NAME            DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
tiller-deploy   1         1         1            1           1h
```

You can upgrade your Tiller instance by running:

```bash
$ helm init --service-account tiller --tiller-namespace tiller --upgrade
```
